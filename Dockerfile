# Dockerfile - for building docker image from existing ruby image.
# Author - Srinivas Jojigiri
# created date - 11/02/2017
# last modified date - 11/06/2017
# Usage: building image - docker build -t web_rails:latest .
#        Running image - docker run -p 3000:8080 -d web_rails:latest
#     docker build takes Dockerfile and executes each and every command, and creates a image with tag name if provided
#     docker run takes the tag name provided and will run the docker container
#     -t tag_name:version - tagging newly built image - optional, by default tag name would be random name
#     -d option is for detach,
#     web_rails is the name of container service - optional, by default all containers are started

# start with the container, use the image with name ruby 2.4 if available in local else get from remote docker container registry
FROM ruby:2.4.0

# Run the commands in the docker container, update the apt and install dependencies for rails
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs

# create new directory and cd to the directory
WORKDIR "/app"

# copy the Gemfile from current directory to the docker container, (from above line to /app)
COPY Gemfile .

# run the command to install all required libraries for project
RUN bundle install

# copy all of the files from current directory to the /app directory, except the files mentioned in .dockerignore
COPY . .

# allow port 3000 from docker container, assuming rails running in that port
EXPOSE 3000

# run the command in the container to start the rails server
CMD ["rails", "s"]
